package repositories

import "food-app/domain/entities"

type UserRepository interface {
	SaveUser(*entities.User) (*entities.User, map[string]string)
	GetUser(uint64) (*entities.User, error)
	GetUsers() ([]entities.User, error)
	GetUserByEmail(string) (*entities.User, map[string]string)
}

type FoodRepository interface {
}
