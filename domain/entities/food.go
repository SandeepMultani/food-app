package entities

import (
	"html"
	"strings"
	"time"
)

// Food represents food entity
type Food struct {
	ID          uint64     `gorm:"primary_key;auto_increment;" json:"id"`
	Title       string     `gorm:"size:100;not null;unique;" json:"title"`
	Description string     `gorm:"text;not null;" json:"description"`
	FoodImage   string     `gorm:"size:255;null;" json:"foodImage"`
	UserID      uint64     `gorm:"size:100;not null;" json:"userId"`
	CreatedAt   time.Time  `gorm:"default:CURRENT_TIMESTAMP;" json:"createdAt"`
	UpdatedAt   time.Time  `gorm:"default:CURRENT_TIMESTAMP;" json:"updatedAt"`
	DeletedAt   *time.Time `gorm:"" json:"deletedAt,omitempty"`
}

// BeforeSave is gorm hook
func (f *Food) BeforeSave() {
	f.Title = html.EscapeString(strings.TrimSpace(f.Title))
}

//Prepare is..
func (f *Food) Prepare() {
	f.Title = html.EscapeString(strings.TrimSpace(f.Title))
	f.CreatedAt = time.Now()
	f.UpdatedAt = time.Now()
}

//Validate is..
func (f *Food) Validate(action string) map[string]string {
	var errMessages = make(map[string]string)

	switch strings.ToLower(action) {
	case "update":
		if f.Title == "" || f.Title == "null" {
			errMessages["title_required"] = "title is required"
		}
		if f.Description == "" || f.Description == "null" {
			errMessages["description_required"] = "description is required"
		}
	default:
		if f.Title == "" || f.Title == "null" {
			errMessages["title_required"] = "title is required"
		}
		if f.Description == "" || f.Description == "null" {
			errMessages["description_required"] = "description is required"
		}
	}

	return errMessages
}
