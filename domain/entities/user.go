package entities

import (
	"food-app/utilities"
	"html"
	"strings"
	"time"

	"github.com/badoux/checkmail"
)

type User struct {
	ID        uint64     `gorm:"primary_key;auto_increment;" json:"id"`
	FirstName string     `gorm:"size:100;not null;" json:"firstName"`
	LastName  string     `gorm:"size:100;not null;" json:"lastName"`
	Email     string     `gorm:"size:100;not null;unique;" json:"email"`
	Password  string     `gorm:"size:100;not null;" json:"password"`
	CreatedAt time.Time  `gorm:"default:CURRENT_TIMESTAMP" json:"createdAt"`
	UpdatedAt time.Time  `gorm:"default:CURRENT_TIMESTAMP" json:"updatedAt"`
	DeletedAt *time.Time `json:"deletedAt,omitempty"`
}

type PublicUser struct {
	ID        uint64 `gorm:"primary_key;auto_increment;" json:"id"`
	FirstName string `gorm:"size:100;not null;" json:"firstName"`
	LastName  string `gorm:"size:100;not null;" json:"lastName"`
}

func (u *User) BeforeSave() error {
	hash, err := utilities.Hash(u.Password)
	if err != nil {
		return err
	}
	u.Password = string(hash)
	return nil
}

type PublicUsers struct {
	Users []PublicUser
}

func (u *User) PublicUser() *PublicUser {
	return &PublicUser{
		ID:        u.ID,
		FirstName: u.FirstName,
		LastName:  u.LastName,
	}
}

func (u *User) Prepare() {
	u.FirstName = html.EscapeString(strings.TrimSpace(u.FirstName))
	u.LastName = html.EscapeString(strings.TrimSpace(u.LastName))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

func (u *User) Validate(action string) map[string]string {
	var errMessage = make(map[string]string)
	switch strings.ToLower(action) {
	case "update":
		if u.Email == "" {
			errMessage["email_required"] = "email required"
		}
		if u.Email != "" {
			if err := checkmail.ValidateFormat(u.Email); err != nil {
				errMessage["invalid_email"] = "email email"
			}
		}

	case "login":
		if u.Password == "" {
			errMessage["password_required"] = "password is required"
		}
		if u.Email == "" {
			errMessage["email_required"] = "email is required"
		}
		if u.Email != "" {
			if err := checkmail.ValidateFormat(u.Email); err != nil {
				errMessage["invalid_email"] = "please provide a valid email"
			}
		}
	case "forgotpassword":
		if u.Email == "" {
			errMessage["email_required"] = "email required"
		}
		if u.Email != "" {
			if err := checkmail.ValidateFormat(u.Email); err != nil {
				errMessage["invalid_email"] = "please provide a valid email"
			}
		}
	default:
		if u.FirstName == "" {
			errMessage["firstname_required"] = "first name is required"
		}
		if u.LastName == "" {
			errMessage["lastname_required"] = "last name is required"
		}
		if u.Password == "" {
			errMessage["password_required"] = "password is required"
		}
		if u.Password != "" && len(u.Password) < 6 {
			errMessage["invalid_password"] = "password should be at least 6 characters"
		}
		if u.Email == "" {
			errMessage["email_required"] = "email is required"
		}
		if u.Email != "" {
			if err := checkmail.ValidateFormat(u.Email); err != nil {
				errMessage["invalid_email"] = "please provide a valid email"
			}
		}
	}
	return errMessage
}
