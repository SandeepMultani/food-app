package constants

// Database constants
const (
	DbDriver   = "DB_DRIVER"
	DbHost     = "DB_HOST"
	DbPassword = "DB_PASSWORD"
	DbUser     = "DB_USER"
	DbName     = "DB_NAME"
	DbPort     = "DB_PORT"
)

// Redis constants
const (
	RedisHost     = "REDIS_HOST"
	RedisPort     = "REDIS_PORT"
	RedisPassword = "REDIS_PASSWORD"
)

// App constants
const (
	AppPort = "APP_PORT"
	AppEnv  = "APP_ENV"
)

// Auth constants
const (
	AccessSecret  = "ACCESS_SECRET"
	RefreshSecret = "REFRESH_SECRET"
)
