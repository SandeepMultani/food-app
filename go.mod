module food-app

go 1.13

require (
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.1.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
