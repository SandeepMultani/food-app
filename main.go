package main

import (
	"food-app/constants"
	"food-app/handlers"
	"food-app/handlers/middlewares"
	"food-app/infrastructure/auth"
	"food-app/infrastructure/persistence"
	"food-app/services"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func init() {
	//load env variables
	if err := godotenv.Load("./app.env"); err != nil {
		log.Fatalln("env variables not found.")
	}
}

func main() {

	dbDriver := os.Getenv(constants.DbDriver)
	dbHost := os.Getenv(constants.DbHost)
	dbPassword := os.Getenv(constants.DbPassword)
	dbUser := os.Getenv(constants.DbUser)
	dbPort := os.Getenv(constants.DbPort)
	dbName := os.Getenv(constants.DbName)

	redisHost := os.Getenv(constants.RedisHost)
	redisPort := os.Getenv(constants.RedisPort)
	redisPassword := os.Getenv(constants.RedisPassword)

	_, _, _ = redisHost, redisPort, redisPassword

	repositories, err := persistence.NewRepositories(dbDriver, dbUser, dbPassword, dbPort, dbHost, dbName)

	if err != nil {
		panic(err)
	}

	defer repositories.Close()
	repositories.Automigrate()

	userService := services.NewUserService(repositories.User)
	authService := auth.NewAuthService()

	userHandler := handlers.NewUsersHandler(userService)
	accountHandler := handlers.NewAccountHandler(authService, repositories.User)

	r := gin.Default()

	r.POST("/users", middlewares.AuthMiddleware(), userHandler.SaveUser)
	r.GET("/users", middlewares.AuthMiddleware(), userHandler.GetUsers)
	r.GET("/users/:userId", middlewares.AuthMiddleware(), userHandler.GetUser)

	r.POST("/login", accountHandler.Login)

	appPort := os.Getenv(constants.AppPort)
	if appPort == "" {
		appPort = "8888"
	}
	log.Fatal(r.Run(":" + appPort))

}
