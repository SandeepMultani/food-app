package services

import (
	"food-app/domain/entities"
	"food-app/domain/repositories"
)

type userService struct {
	ur repositories.UserRepository
}

type UserServiceInterface interface {
	SaveUser(*entities.User) (*entities.PublicUser, map[string]string)
	GetUser(uint64) (*entities.PublicUser, error)
	GetUsers() (*entities.PublicUsers, error)
}

func NewUserService(ur repositories.UserRepository) *userService {
	return &userService{ur}
}

//to check the interface implementation
var _ UserServiceInterface = &userService{}

func (u *userService) SaveUser(user *entities.User) (*entities.PublicUser, map[string]string) {
	usr, validationErr := u.ur.SaveUser(user)
	if validationErr != nil {
		return nil, validationErr
	}
	return usr.PublicUser(), validationErr
}

func (u *userService) GetUser(userID uint64) (*entities.PublicUser, error) {
	usr, err := u.ur.GetUser(userID)
	if err != nil {
		return nil, err
	}

	return usr.PublicUser(), err
}

func (u *userService) GetUsers() (*entities.PublicUsers, error) {
	users, err := u.ur.GetUsers()
	if err != nil {
		return nil, err
	}

	pubUsers := entities.PublicUsers{
		Users: make([]entities.PublicUser, len(users)),
	}
	for index, user := range users {
		pubUsers.Users[index] = *user.PublicUser()
	}
	return &pubUsers, nil
}
