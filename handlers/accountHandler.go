package handlers

import (
	"food-app/domain/entities"
	"food-app/domain/repositories"
	"food-app/infrastructure/auth"
	"food-app/utilities"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AccountHandler struct {
	auth auth.AuthService
	ur   repositories.UserRepository
}

func NewAccountHandler(auth auth.AuthService, ur repositories.UserRepository) *AccountHandler {
	return &AccountHandler{
		auth: auth,
		ur:   ur,
	}
}

func (ah *AccountHandler) Login(c *gin.Context) {
	var cred *auth.Credentials
	if err := c.ShouldBindJSON(&cred); err != nil {
		c.JSON(http.StatusBadRequest, "invalid data")
		return
	}

	//check if password is valid
	usr, validationErr := verifyPassword(&ah.ur, cred.Email, cred.Password)
	if validationErr != nil {
		c.JSON(http.StatusBadRequest, "invalid crendentials")
		return
	}

	ts, tErr := ah.auth.NewToken(usr)
	if tErr != nil {
		c.JSON(http.StatusInternalServerError, tErr.Error())
		return
	}

	res := make(map[string]interface{})
	res["access_token"] = ts

	c.JSON(http.StatusOK, res)
}

func verifyPassword(r *repositories.UserRepository, email string, password string) (*entities.User, map[string]string) {
	usr, validationErr := (*r).GetUserByEmail(email)
	if validationErr != nil {
		return nil, validationErr
	}
	if utilities.VerifyPassword(usr.Password, password) != nil {
		var err = map[string]string{}
		err["incorrect_password"] = "incorrect password"
		return nil, err
	}
	return usr, nil
}
