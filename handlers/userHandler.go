package handlers

import (
	"food-app/domain/entities"
	"food-app/services"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us services.UserServiceInterface
}

func NewUsersHandler(us services.UserServiceInterface) *UserHandler {
	return &UserHandler{
		us: us,
	}
}

func (uh *UserHandler) GetUsers(c *gin.Context) {
	users, err := uh.us.GetUsers()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, users.Users)
}

func (uh *UserHandler) GetUser(c *gin.Context) {
	userId, err := strconv.ParseUint(c.Param("userId"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	user, err := uh.us.GetUser(userId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, *user)
}

func (uh *UserHandler) SaveUser(c *gin.Context) {
	var user entities.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"invalid_json": "invalid json",
		})
		return
	}

	validateErr := user.Validate("")
	if len(validateErr) > 0 {
		c.JSON(http.StatusBadRequest, validateErr)
		return
	}

	newUser, err := uh.us.SaveUser(&user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusCreated, *newUser)
}
