package middlewares

import (
	"food-app/infrastructure/auth"
	"net/http"

	"github.com/gin-gonic/gin"
)

const XAuthToken = "x-auth-token"

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get(XAuthToken)
		if token == "" {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"error":  "invalid token",
			})
			c.Abort()
			return
		}
		authSer := auth.NewAuthService()
		_, err := authSer.VerifyToken(token)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"error":  "invalid token",
			})
			c.Abort()
			return
		}
		c.Next()
	}
}
