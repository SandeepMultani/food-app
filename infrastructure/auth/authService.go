package auth

import (
	"errors"
	"food-app/domain/entities"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var jwtKey = []byte("key")

type AuthService interface {
	NewToken(*entities.User) (string, error)
	VerifyToken(string) (*Claims, error)
	RefreshToken(string) (string, error)
}

type Credentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Claims struct {
	UserID uint64 `json:"userId"`
	Email  string `json:"email"`
	jwt.StandardClaims
}

type JwtAuthService struct {
}

func NewAuthService() AuthService {
	return &JwtAuthService{}
}

var _ AuthService = &JwtAuthService{}

func (auth *JwtAuthService) NewToken(u *entities.User) (string, error) {
	expirationTime := time.Now().Add(5 * time.Minute)
	claims := &Claims{
		UserID: u.ID,
		Email:  u.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  time.Now().Unix(),
			Audience:  "aud",
			Issuer:    "isu",
		},
	}

	tokenString, err := newTokenWithClaims(claims)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (auth *JwtAuthService) VerifyToken(token string) (*Claims, error) {
	claims := &Claims{}
	tkn, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		return nil, err
	}
	if !tkn.Valid {
		return nil, errors.New("token not valid")
	}
	return claims, nil
}

func (auth *JwtAuthService) RefreshToken(token string) (string, error) {
	claims, err := auth.VerifyToken(token)
	if err != nil {
		return "", nil
	}

	claims.ExpiresAt = time.Now().Add(5 * time.Minute).Unix()

	tokenString, err := newTokenWithClaims(claims)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func newTokenWithClaims(claims *Claims) (string, error) {
	newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := newToken.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
