package persistence

import (
	"errors"
	"food-app/domain/entities"
	"food-app/domain/repositories"

	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{db}
}

var _ repositories.UserRepository = &UserRepository{}

func (r *UserRepository) GetUsers() ([]entities.User, error) {
	var usrs []entities.User
	err := r.db.Debug().Find(&usrs).Error
	if err != nil {
		return nil, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("users not found")
	}

	return usrs, nil
}

func (r *UserRepository) GetUserByEmail(email string) (*entities.User, map[string]string) {
	var user entities.User
	dbErr := map[string]string{}
	err := r.db.Debug().Where("email = ?", email).Take(&user).Error
	if gorm.IsRecordNotFoundError(err) {
		dbErr["no_user"] = "user not found"
		return nil, dbErr
	}
	if err != nil {
		dbErr["db_error"] = "database error" + err.Error()
		return nil, dbErr
	}

	return &user, nil
}

func (r *UserRepository) GetUser(ID uint64) (*entities.User, error) {
	var user entities.User
	err := r.db.Debug().Where("id = ?", ID).Take(&user).Error
	if gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("user not found")
	}
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *UserRepository) SaveUser(user *entities.User) (*entities.User, map[string]string) {
	dbErr := map[string]string{}
	err := r.db.Debug().Create(&user).Error
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok && pqErr.Code.Name() == "unique_violation" {
			dbErr["email_taken"] = "email already taken"
			return nil, dbErr
		}

		dbErr["db_error"] = "database error" + err.Error()
		return nil, dbErr
	}

	return user, nil
}
