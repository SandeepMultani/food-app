package persistence

import (
	"fmt"
	"food-app/domain/entities"
	"food-app/domain/repositories"

	"github.com/jinzhu/gorm"
	// postgres driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Repositories struct {
	User repositories.UserRepository
	Food repositories.FoodRepository
	db   *gorm.DB
}

func NewRepositories(dbDriver, dbUser, dbPassword, dbPort, dbHost, dbName string) (*Repositories, error) {
	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, dbPort, dbUser, dbName, dbPassword)

	db, err := gorm.Open(dbDriver, DBURL)
	if err != nil {
		return nil, err
	}
	db.LogMode(true)

	return &Repositories{
		User: NewUserRepository(db),
		//Food: NewFoodRepository(db),
		db: db,
	}, nil
}

func (s *Repositories) Close() error {
	return s.db.Close()
}

func (s *Repositories) Automigrate() error {
	return s.db.AutoMigrate(&entities.User{}, &entities.Food{}).Error
}
